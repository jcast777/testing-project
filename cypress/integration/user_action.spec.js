describe('User Action', () => {

    it('register and login user', () => {

        cy.visit('/register');

        cy.get('#name')
            .type('John Doe').should('have.value', 'John Doe')

        cy.get('#email')
            .type('test@email.com').should('have.value', 'test@email.com')

        cy.get('#password')
            .type('password123').should('have.value', 'password123')
        
        cy.get('#password_confirmation')
            .type('password123').should('have.value', 'password123')

        cy.get('#register-btn').click()

    });

    it('logout user', () => {

        cy.get('#account-btn').click()

        cy.get('#logout-link').click()

    });

    it('relogin user', () => {

        cy.visit('/login');

        cy.get('#email')
            .type('test@email.com').should('have.value', 'test@email.com')

        cy.get('#password')
            .type('password123').should('have.value', 'password123')
        
        cy.get('#login-btn').click()

    });

});
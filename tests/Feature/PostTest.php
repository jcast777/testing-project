<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function test_user_cant_access_create_route_while_not_loggedin()
    {
        $this->get('/dashboard/posts/create')->assertRedirect('/login');
    }

    public function test_user_cant_access_edit_route_while_not_loggedin()
    {
        $this->get('/dashboard/posts/1/edit')->assertRedirect('/login');
    }

    public function test_user_cant_access_store_route_while_not_loggedin()
    {
        $this->post('/dashboard/posts')->assertRedirect('/login');
    }

    public function test_user_cant_access_update_route_while_not_loggedin()
    {
        $this->put('/dashboard/posts/1')->assertRedirect('/login');
    }

    public function test_post_can_be_stored()
    {
        $this->actingAs($user = User::factory()->create());
        
        $this->assertAuthenticated();

        $posts = Post::factory()->create([
            'user_id' => $user->id,
        ]);
       
        $this->post('/dashboard/posts', $posts->toArray())
            ->assertSessionHasErrors();
        
        $this->assertDatabaseHas('posts', [
            'title' => $posts->title,
            'content' => $posts->content,
            'deleted_at' => null
        ]);
    }

    public function test_post_can_be_updated()
    {
        $this->actingAs($user = User::factory()->create());
        
        $this->assertAuthenticated();

        $posts = Post::factory()->create([
            'user_id' => $user->id,
        ]);
       
        $updates = [
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
        ];
        
        $this->put('/dashboard/posts/'. $posts->id, $updates);
        
        $this->assertDatabaseHas('posts', $updates);
    }

    public function test_post_can_be_seen()
    {
        $this->actingAs($user = User::factory()->create());

        $posts = Post::factory()->create([
            'user_id' => $user->id,
            'title' => 'New post'
        ]);

        $this->get('/posts')->assertSee('New post');
    }
}
